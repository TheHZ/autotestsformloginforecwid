using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Configuration;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;

namespace TestEcwid
{
    [TestClass]
    public class LoginFormTestSuit
    {
        private IWebDriver driver;
        private LoginPage page;
        private ConfigParser config;

        private const int TIME_FOR_TEST = 120000;

        [TestInitialize]
        public void BeforeTest()
        {
            config = new ConfigParser();
            int delay = int.Parse((config.Get("delay")));

            driver = new ChromeDriver(Directory.GetCurrentDirectory());
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(delay);
            driver.Manage().Window.Maximize();

            string urlLogin = config.Get("urlLogin");
            driver.Navigate().GoToUrl(urlLogin);
            page = new LoginPage(driver, delay);
        }

        [TestCleanup]
        public void AfterTest()
        {
            driver.Quit();
        }

        /*1)�������������� � ��������� ������ ������. 
        � ���� ����� ������ - dddd73@yandex.ru
        � ���� ������ ������ - Wizard73 
        2)������ �� ������ "�����"
        ��: ���� ��������, ����������� ������� �� ����������� ������*/
        [TestCategory("LoginFormPositiveTest")]
        [Timeout(TIME_FOR_TEST)]
        [TestMethod]
        public void CorrectEnterLoginPassword()
        {
            string correctLogin = config.Get("correctLogin");
            string correctPassword = config.Get("correctPassword");

            page.Login(correctLogin, correctPassword);
            Assert.IsTrue(page.ValidationCorrectEntrance(), "���� �� ��� �����������");
        }

        /*1)�������������� � ��������� ������ ������, �� � ������� ��� ����� ��������
        � ���� ����� ������ - dddd73@yandex.ru
        � ���� ������ ������ ������ ������, �� ��� ����� �������� - wizard73
        2)������ �� ������ "�����"
        ��: ������� ��������� "������������ ������. ��� �������������� ������ �������������� 
        ������� ������� ������?�"*/
        [TestCategory("LoginFormNegativeTest")]
        [Timeout(TIME_FOR_TEST)]
        [TestMethod]
        public void PasswordExcludingRegistr()
        {
            string correctLogin = config.Get("correctLogin");
            string noRegistrPassword = config.Get("noRegistrPassword");

            page.Login(correctLogin, noRegistrPassword);
            Assert.IsTrue(page.IncorectPassword(), "��������� �� ������ �� ���������");
        }

        /*1)� ���� ����� ������ - dddd73@yandex.ru
        � ���� ������ ������ �������� ������ - incorrectPassword
        2)������ �� ������ "�����"
        ��: ������� ��������� "������������ ������. ��� �������������� ������ �������������� 
        ������� ������� ������?�"*/
        [TestCategory("LoginFormNegativeTest")]
        [Timeout(TIME_FOR_TEST)]
        [TestMethod]
        public void IncorrectPassword()
        {
            string correctLogin = config.Get("correctLogin");
            string incorrectPassword = config.Get("incorrectPassword");

            page.Login(correctLogin, incorrectPassword);
            Assert.IsTrue(page.IncorectPassword());
        }

        /*1)� ���� ����� ������ ����� � �� �������� ������� - dddd73
        � ���� ������ ������ ������������ ������ - Wizard73
        2)������ �� ������ "�����"
        ��: ������� ��������� "����������, ������� ���������� email"*/
        [TestCategory("LoginFormNegativeTest")]
        [Timeout(TIME_FOR_TEST)]
        [TestMethod]
        public void IncorrectEmail()
        {
            string incorrectMail = config.Get("incorrectMail");
            string correctPassword = config.Get("correctPassword");

            page.Login(incorrectMail, correctPassword);
            Assert.IsTrue(page.IncorrectEmail());
        }

        /*1)� ���� ����� ������ �������������������� email - incorrectMail@yandex.ru � ���� 
        ������ ������ - Wizard73
        2)������ �� ������ "�����"
        ��: ������� ��������� "��� ��������� � ����� email. ���� �� �� ������� email ������ 
        ��������, �������� ��� �� login-issues@ecwid.com."
        */
        [TestCategory("LoginFormNegativeTest")]
        [Timeout(TIME_FOR_TEST)]
        [TestMethod]
        public void NotExistEmail()
        {
            string notExistMail = config.Get("notExistMail");
            string correctPassword = config.Get("correctPassword");

            page.Login(notExistMail, correctPassword);
            Assert.IsTrue(page.NotExistMail());
        }

        /*1)� ���� ������ ������ - Wizard73
        2)������ �� ������ "�����"
        ��: ������� ��������� "����������, ������� ���������� email"*/
        [TestCategory("LoginFormNegativeTest")]
        [Timeout(TIME_FOR_TEST)]
        [TestMethod]
        public void NotEnterEmail()
        {
            string emptyMail = "";
            string correctPassword = config.Get("correctPassword");

            page.Login(emptyMail, correctPassword);
            Assert.IsTrue(page.IncorrectEmail());
        }

        /*1)� ���� ����� ������ - dddd73@yandex.ru
        2)������ �� ������ "�����"
        ��: ������� ��������� - "����������, ������� ��� ������"*/
        [TestCategory("LoginFormNegativeTest")]
        [Timeout(TIME_FOR_TEST)]
        [TestMethod]
        public void NotEnterPassword()
        {
            string correctLogin = config.Get("correctLogin");
            string emptyPassword = "";

            page.Login(correctLogin, emptyPassword);
            Assert.IsTrue(page.WithoutPassword());
        }

        /*1)�������� ���� login � password �������
         2)������ �� ������ "�����"
         ��: ������� ���� - "����������, ������� ���������� email"*/
        [TestCategory("LoginFormNegativeTest")]
        [Timeout(TIME_FOR_TEST)]
        [TestMethod]
        public void WithoutDataEntry()
        {
            string emptyMail = "";
            string emptyPassword = "";

            page.Login(emptyMail, emptyPassword);
            Assert.IsTrue(page.IncorrectEmail());
        }

        /*1)������ �� ������ -  "������ ������?"
         ��: ������� ����� � �������������� ������*/
        [TestCategory("LoginFormPositiveTest")]
        [Timeout(TIME_FOR_TEST)]
        [TestMethod]
        public void ForgotYourPassword()
        {
            Assert.IsTrue(page.ForgotYourPassword());
        }
    }
}
