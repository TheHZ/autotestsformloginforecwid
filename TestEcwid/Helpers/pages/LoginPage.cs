﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace TestEcwid
{
    class LoginPage
    {
        private IWebDriver driver;
        private int delay;

        private By loginEdit = By.XPath("//input[@name='email']");
        private By passwordEdit = By.XPath("//input[@name='password']");
        private By buttonEnter = By.CssSelector("div.block-view-on > form > div > button");
        private By checkLogin = By.CssSelector("div.settings-page__header > div > h1");
        private By errorMessange = By.CssSelector("div.bubble.notitle  div.bbl-text > div");
        private By checkForgotYourPassword = By.CssSelector(
            "body > div.login-container div.reset-view.block-view-on > h3 > div");
        private By forgotYourPassword = By.CssSelector(String.Format(
            "div.block-view-on > form > div > p:nth-child(3) > a:nth-child(1)"));

        public LoginPage(IWebDriver driver, int delay)
        {
            this.driver = driver;
            this.delay = delay;
        }

        public void Login(string login, string password)
        {
            driver.FindElement(loginEdit).SendKeys(login);
            Console.WriteLine("\r\nЗначаение  для ввода в поле логин - " + login);
            driver.FindElement(passwordEdit).SendKeys(password);
            Console.WriteLine("\r\nЗначаение  для ввода в поле пароля - " + password);
            driver.FindElement(buttonEnter).Click();
        }

        public bool WithoutPassword()
        {
            string textForCheck = "Пожалуйста, введите ваш пароль";

            return OutputMessange(errorMessange, textForCheck);
        }

        public bool NotExistMail()
        {
            string textForCheck = "Нет магазинов с таким email.";

            return OutputMessange(errorMessange, textForCheck);
        }

        public bool IncorrectEmail()
        {
            string textForCheck = "Пожалуйста, введите правильный email";

            return OutputMessange(errorMessange, textForCheck);
        }

        public bool IncorectPassword()
        {
            string textForCheck = "Неправильный пароль. Для восстановления пароля";

            return OutputMessange(errorMessange, textForCheck);
        }

        public bool ForgotYourPassword()
        {
            driver.FindElement(forgotYourPassword).Click();

            IWebElement element = WaitElement(checkForgotYourPassword);
            string headerTextForVerification = "Восстановление доступа";

            if (element.Text.Contains(headerTextForVerification))
            {
                Console.WriteLine("\r\nФорма востановления пароля появилась");
                return true;
            }
            else
            {
                Console.WriteLine("\r\nФорма востановления пароля не появилась");
                return false;
            }
        }

        public bool ValidationCorrectEntrance()
        {
            if (driver.FindElement(checkLogin).Displayed)
            {
                Console.WriteLine("\r\nВход успешно выполнен");
                return true;
            }
            else
            {
                Console.WriteLine("\r\nВход не выполнен");
                return false;
            }
        }

        private bool OutputMessange(By locator, string textForCheck)
        {
            IWebElement element = WaitElement(locator);
            if (element.Text.Contains(textForCheck))
            {
                Console.WriteLine("\r\nСообщение - '" + textForCheck + "'  появилось");
                return true;
            }
            else
            {
                Console.WriteLine("\r\nСообщение " + textForCheck + " не появилось");
                return false;
            }
        }

        private IWebElement WaitElement(By locator)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(delay));
            return wait.Until(driver => 
                driver.FindElement(locator).Displayed ? driver.FindElement(locator) : null);
        }
    }
}

