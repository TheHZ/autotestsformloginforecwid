using Microsoft.Extensions.Configuration;
using System.IO;

namespace TestEcwid
{
    class ConfigParser
    {
        private static IConfiguration Configuration { get; set; }

        public ConfigParser()
        {
			var builder = new ConfigurationBuilder().AddJsonFile("AppConfig.json");
            string i = Directory.GetCurrentDirectory();

            Configuration = builder.Build();
        }

        public string Get(string valueConfig)
        {
            return Configuration[valueConfig];
        }
    }
}
